FROM openjdk:11-jre-slim-sid
EXPOSE 8080
WORKDIR /app
COPY ./.env.develop /app/.env.develop
COPY ./archive /app/archive
COPY ./build/libs/*.jar /app/converter-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "converter-0.0.1-SNAPSHOT.jar"]