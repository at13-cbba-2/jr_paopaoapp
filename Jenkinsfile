pipeline {
  agent any
  environment {
    DOCKER_USERNAME = credentials('docker_hub_nickname')
    DOCKER_HUB_PASSWORD = credentials('docker_hub_passw')
    SONAR_TOKEN = credentials('sonar_token')
    EMAIL = 'jr.bestone17@gmail.com'
    COMMIT_INFO = sh(returnStdout: true, script: "git log --format=medium -1 ${GIT_COMMIT}").trim()
  }
  stages {
    stage('commit') {
      steps {
        sh 'chmod +x gradlew'
        sh './gradlew clean build'
      }
      post {
        always {
            archiveArtifacts artifacts: 'build/libs/*.jar', fingerprint: true
            junit 'build/test-results/test/*.xml'
        }
      }
    }
    stage('code quality') {
      steps {
        sh './gradlew sonarqube'
      }
    }
    stage('package') {
      steps {
        sh 'docker build -t converter:1.8 .'
      }
    }
    stage('publish') {
      steps {
        sh 'docker login -u "${DOCKER_USERNAME}" -p "${DOCKER_HUB_PASSWORD}"'
        sh 'docker image tag converter:1.8 diprodri/converter:1.8'
        sh 'docker image push diprodri/converter:1.8'
        sh 'echo docker login, tag, push'
      }
    }
    stage('deploy to dev') {
      steps {
        sh 'docker-compose up --force-recreate -d'
      }
    }
  }
  post{
    always{
      mail to: "${EMAIL}",
      subject: "Build #${currentBuild.number} with Status: ${currentBuild.result}", 
      body: "Build: #${currentBuild.number}\n\nCommit:\n ${COMMIT_INFO}\n\nLog:\n${currentBuild.absoluteUrl}console"
      }
  }
}
